#include "Camera.h"
#include "ReadingHelper.h"
#include<string>
using namespace std;

Camera::Camera()
{
}


Camera::~Camera()
{
}

int Camera::ExtractImageHeight()
{
	int res;
	res = (int)(tan(glm::radians(_fov / 2)) * _focal_length);
	res *= 2;
	return res;
}

int Camera::ExtractImageWidth()
{
	int res;
	res = (int)(tan(glm::radians(_fov / 2)) * _focal_length * _aspect_ratio);
	res *= 2;
	return res;
}

Ray Camera::GetRayFromImageCoord(float x, float y)
{
	Ray r;
	r.origin = _pos;
	r.direction = glm::normalize(glm::vec3(x, y, _pos.z - _focal_length) - _pos);
	return r;
}

void Camera::LoadFromFile(std::ifstream & fs)
{
	string tempLine;
	for (int i = 1; i <= 4; i++) {
		getline(fs, tempLine, ':');

		if (tempLine == "pos") {
			_pos = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "fov") {
			_fov = ReadingHelper::ExtractFloat(fs);
		}
		else if (tempLine == "f") {
			_focal_length = ReadingHelper::ExtractFloat(fs);
		}
		else if (tempLine == "a") {
			_aspect_ratio = ReadingHelper::ExtractFloat(fs);
		}
	}
}


