#pragma once
#include "SceneObject.h"
#include "glm.hpp"
#include "Ray.h"
class Light :
	public SceneObject
{
public:
	Light();
	~Light();

	glm::vec3 GetDirectionFromPoint(glm::vec3 origin);
	glm::vec3 GetDirectionFromPoint(glm::vec3 origin, float shiftX, float shiftY);
	glm::vec3 GetColor() { return _color; };
	bool IsBeforeIntersection(Ray r, glm::vec3 intersectionPoint);
private:
	glm::vec3 _pos;
	glm::vec3 _color;

	virtual void LoadFromFile(std::ifstream & fs) override;
};

