#pragma once
#include "ReflectingObject.h"
class Triangle :
	public ReflectingObject
{
public:
	Triangle();
	~Triangle();
private:
	glm::vec3 _v1;
	glm::vec3 _v2;
	glm::vec3 _v3;
	glm::vec3 _norm;

	virtual void LoadFromFile(std::ifstream & fs) override;
	virtual float Intersect(Ray ray) override;

	// Inherited via ReflectingObject
	virtual glm::vec3 GetNormal(glm::vec3 point) override;
};

