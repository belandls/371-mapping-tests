#include "Triangle.h"
#include "ReadingHelper.h"
#include<string>
using namespace std;


Triangle::Triangle()
{
}


Triangle::~Triangle()
{
}

void Triangle::LoadFromFile(std::ifstream & fs)
{
	string tempLine;
	for (int i = 1; i <= 3; i++) {
		getline(fs, tempLine, ':');

		if (tempLine == "v1") {
			_v1 = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "v2") {
			_v2 = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "v3") {
			_v3 = ReadingHelper::ExtractVector(fs);
		}
	}

	ReflectingObject::LoadFromFile(fs);

	_norm = glm::normalize(glm::cross(_v2 - _v1, _v3 - _v1));
}

float Triangle::Intersect(Ray ray){

	float d = glm::dot(ray.direction, _norm);
	if (abs(d) < 1e-6){
		return numeric_limits<float>::infinity();
	}
	else{
		float t = glm::dot(_v1 - ray.origin, _norm) / d;
		glm::vec3 p = ray.origin + t*ray.direction;

		float d1, d2, d3;
		d1 = glm::dot(_norm, glm::cross(_v2 - _v1, p - _v1));
		d2 = glm::dot(_norm, glm::cross(_v3 - _v2, p - _v2));
		d3 = glm::dot(_norm, glm::cross(_v1 - _v3, p - _v3));

		if (d1 > 0 && d2 > 0 && d3 > 0){
			return t;
		}
		else{
			return numeric_limits<float>::infinity();
		}

	}


}

glm::vec3 Triangle::GetNormal(glm::vec3 point)
{
	return _norm;
}
