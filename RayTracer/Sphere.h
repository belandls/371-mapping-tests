#pragma once
#include "ReflectingObject.h"
class Sphere :
	public ReflectingObject
{
public:
	Sphere();
	~Sphere();
private:
	glm::vec3 _center;
	float _radius;

	virtual void LoadFromFile(std::ifstream & fs) override;
	virtual float Intersect(Ray ray) override;

	// Inherited via ReflectingObject
	virtual glm::vec3 GetNormal(glm::vec3 point) override;
};

