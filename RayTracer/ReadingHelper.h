#pragma once
#include <fstream>
#include "glm.hpp"

class ReadingHelper
{
private:
	ReadingHelper();

public:

	static glm::vec3 ExtractVector(std::ifstream &fs);
	static float ExtractFloat(std::ifstream &fs);
	static int ExtractInt(std::ifstream &fs);
};

