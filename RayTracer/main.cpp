#include <string>
#include "CImg.h"
#include "glm.hpp"

using namespace std;

int main(void){

	int width, height;
	height = 300;
	width = 300;

	cimg_library::CImg<float> image(height, width, 1, 3, 0.0f);
	float** noise = new float*[height];
	for (int i = 0; i < height; ++i) {
		noise[i] = new float[width];
	}
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			noise[i][j] = (float)rand() / RAND_MAX;
		}
	}

	 
	float** smoothNoise = new float*[height];
	for (int i = 0; i < height; ++i) {
		smoothNoise[i] = new float[width];
	}
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			smoothNoise[i][j] = 0.0f;
		}
	}
	for (int k = 1; k <= 32; k *= 2) {
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				double x = i / (double)k;
				double y = j / (double)k;

				double fracX = x - int(x);
				double fracY = y - int(y);

				int x1 = (int(x) + width) % width;
				int y1 = (int(y) + height) % height;

				int x2 = (x1 + width - 1) % width;
				int y2 = (y1 + height - 1) % height;

				double value = 0.0;
				value += fracX * fracY * noise[y1][x1];
				value += (1 - fracX) * fracY * noise[y1][x2];
				value += fracX * (1 - fracY) * noise[y2][x1];
				value += (1 - fracX) * (1 - fracY) * noise[y2][x2];

				smoothNoise[i][j] += (k/32.0) *value;
			}
		}
	}

	float** sinus = new float*[height];
	for (int i = 0; i < height; ++i) {
		sinus[i] = new float[width];
	}
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			sinus[i][j] = 0.0f;
		}
	}
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			double test = j*0.0 / width + i * 10.0 / height + 0.6 * smoothNoise[i][j];
			sinus[i][j] = 1.0f -  fabs(sin(test * 3.14159));
		}
	}



	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			*image.data(i, j, 0, 0) = sinus[i][j];
			*image.data(i, j, 0, 1) = sinus[i][j];
			*image.data(i, j, 0, 2) = sinus[i][j];
		}
	}
	
	cimg_library::CImgDisplay disp(image, "lol");
	while (!disp.is_closed());
	disp.wait();

	/*Scene s;
	s.LoadFromFile("scene.txt");
	s.Trace("allo.bmp");*/
	/*ifstream sceneFileStream("scene.txt");
	if (sceneFileStream.is_open()) {
		
		int numOfObjects = ReadingHelper::ExtractInt(sceneFileStream);

		for (int i = 0; i < numOfObjects; ++i) {

			SceneObject* temp = NULL;
			string objName;
			getline(sceneFileStream, objName);

			if (objName == "camera") {
				temp = new Camera();
			}

			temp->LoadFromFile(sceneFileStream);
		}
		
		cimg_library::CImg<float> image(_width, _height, 1, 3, 0.0f);
		image.normalize(0, 255);
		image.save(destFileName.c_str());
		cimg_library::CImgDisplay disp(image, "lol");
		while (!disp.is_closed());
		disp.wait();

	}*/

	return 0;
}