#pragma once
#include <fstream>
#include "glm.hpp"
class SceneObject
{
public:
	SceneObject();
	~SceneObject();

	virtual void LoadFromFile(std::ifstream &fs) = 0;

protected:
	
	
};

