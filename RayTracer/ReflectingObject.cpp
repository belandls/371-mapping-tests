#include "ReflectingObject.h"
#include "ReadingHelper.h"
#include<string>
using namespace std;


ReflectingObject::ReflectingObject()
{
}


ReflectingObject::~ReflectingObject()
{
}


void ReflectingObject::ApplyColorContribution(glm::vec3 & color, glm::vec3 lightColor, glm::vec3 l, glm::vec3 v, glm::vec3 intersectionPoint)
{
	glm::vec3 norm = GetNormal(intersectionPoint);
	glm::vec3 reflectedVector = 2 * glm::dot(l, norm)*norm - l;

	float dLN = glm::dot(l, norm);
	float dRV = glm::dot(reflectedVector, v);
	if (dLN < 0) {
		dLN = 0.0f;
	}
	if (dRV < 0) {
		dRV = 0.0f;
	}
	
	//color.r += lightColor.r * (_dif_col.r * dLN + _spec_col.r * pow(dRV, _shininess_factor));
	//color.g += lightColor.g * (_dif_col.g * dLN + _spec_col.g * pow(dRV, _shininess_factor));
	//color.b += lightColor.b * (_dif_col.b * dLN + _spec_col.b * pow(dRV, _shininess_factor));
	color += lightColor * ( _dif_col * dLN + _spec_col * pow( dRV , _shininess_factor) );
}

void ReflectingObject::ApplyAmbientColorAndClamp(glm::vec3 & color)
{
	color += _amb_col;
	if (color.r > 1.0f) {
		color.r = 1.0f;
	}
	if (color.g > 1.0f) {
		color.g = 1.0f;
	}
	if (color.b > 1.0f) {
		color.b = 1.0f;
	}
}

glm::vec3 ReflectingObject::GetReflectedVector(glm::vec3 incoming, glm::vec3 intersection)
{
	glm::vec3 norm = GetNormal(intersection);
	return 2 * glm::dot(incoming, norm)*norm - incoming;
}

void ReflectingObject::LoadFromFile(std::ifstream & fs)
{
	string tempLine;
	for (int i = 1; i <= 4; i++) {
		getline(fs, tempLine, ':');

		if (tempLine == "amb") {
			_amb_col = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "dif") {
			_dif_col = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "spe") {
			_spec_col = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "shi") {
			_shininess_factor = ReadingHelper::ExtractFloat(fs);
		}
	}
}
