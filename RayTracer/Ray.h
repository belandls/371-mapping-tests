#pragma once
#include "glm.hpp"

struct Ray{
	glm::vec3 origin;
	glm::vec3 direction;

	Ray();
	Ray(glm::vec3 origin, glm::vec3 direction);

	glm::vec3 LinearValue(float t);
};