#pragma once
#include "SceneObject.h"
#include "glm.hpp"
#include "Ray.h"
class ReflectingObject :
	public SceneObject
{
public:
	ReflectingObject();
	~ReflectingObject();

	virtual float Intersect(Ray ray) = 0;
	
	void ApplyColorContribution(glm::vec3 &color,glm::vec3 lightColor, glm::vec3 l, glm::vec3 v, glm::vec3 intersectionPoint);
	void ApplyAmbientColorAndClamp(glm::vec3 &color);
	glm::vec3 GetReflectedVector(glm::vec3 incoming, glm::vec3 intersection);
	
	virtual glm::vec3 GetNormal(glm::vec3 point) = 0;

protected:
	virtual void LoadFromFile(std::ifstream & fs) override;
	
private:
	glm::vec3 _amb_col;
	glm::vec3 _dif_col;
	glm::vec3 _spec_col;
	float _shininess_factor;
};

