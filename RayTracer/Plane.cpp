#include "Plane.h"
#include "ReadingHelper.h"
#include<string>

using namespace std;

Plane::Plane()
{
}


Plane::~Plane()
{
}

void Plane::LoadFromFile(std::ifstream & fs)
{
	string temp;
	for (int i = 1; i <= 2; i++) {
		getline(fs, temp, ':');

		if (temp == "nor") {
			_norm = ReadingHelper::ExtractVector(fs);
		}
		else if (temp == "pos") {
			_point = ReadingHelper::ExtractVector(fs);
		}
	}


	ReflectingObject::LoadFromFile(fs);
}

float Plane::Intersect(Ray ray){

	float d = glm::dot(ray.direction, _norm);
	if (abs(d) < 1e-6){
		return numeric_limits<float>::infinity();
	}
	else{
		return glm::dot(_point - ray.origin,_norm) / d;
	}

}

glm::vec3 Plane::GetNormal(glm::vec3 point)
{
	return _norm;
}
