#pragma once
#include "SceneObject.h"
#include "glm.hpp"
#include "Ray.h"
class Camera :
	public SceneObject
{
public:
	Camera();
	~Camera();

	int ExtractImageHeight();
	int ExtractImageWidth();
	Ray GetRayFromImageCoord(float x, float y);
	

private:
	
	glm::vec3 _pos;
	float _fov;
	float _focal_length;
	float _aspect_ratio;

	// Inherited via SceneObject
	virtual void LoadFromFile(std::ifstream & fs) override;

};
