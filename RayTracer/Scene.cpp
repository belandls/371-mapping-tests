#include "Scene.h"
#include "ReadingHelper.h"
#include "Triangle.h"
#include "Plane.h"
#include "Sphere.h"
#include "CImg.h"
#include  <fstream>
using namespace std;

Scene::Scene()
{
	_cam = NULL;
}


Scene::~Scene()
{
}

void Scene::LoadFromFile(string fn) {

	ifstream sceneFileStream(fn);
	if (sceneFileStream.is_open()) {

		int numOfObjects = ReadingHelper::ExtractInt(sceneFileStream);

		for (int i = 0; i < numOfObjects; ++i) {

			SceneObject* temp = NULL;
			string objName;
			getline(sceneFileStream, objName);

			if (objName == "camera") {
				temp = new Camera();
				_cam = (Camera*)temp;
			}
			else if (objName == "light") {
				temp = new Light();
				_lights.push_back((Light*)temp);
			}
			else {
				if (objName == "triangle") {
					temp = new Triangle();
				}
				else if (objName == "sphere") {
					temp = new Sphere();
				}
				else if (objName == "plane") {
					temp = new Plane();
				}
				_objects.push_back((ReflectingObject*)temp);
			}
			temp->LoadFromFile(sceneFileStream);
		}

		sceneFileStream.close();
	}
	if (_cam != NULL) {

		_height = _cam->ExtractImageHeight();
		_width = _cam->ExtractImageWidth();

	}

}

void Scene::Trace(std::string destFileName)
{
	cimg_library::CImg<float> image(_width, _height, 1, 3, 0.0f);

	

	for (int y = _height - 1; y >= 0; --y) {
		for (int x = 0; x < _width; ++x) {

			int avgCounter = 0;
			glm::vec3 avgColor(0.0f);
			for (float yJitter = 0.20f; yJitter <= 0.80f; yJitter += 0.20f) {
				for (float xJitter = 0.20f; xJitter <= 0.80f; xJitter += 0.20f) {
					++avgCounter;
					glm::vec3 pixelColor(0.0f);
					float centeredImageX, centeredImageY;
					centeredImageX = (float)x - _width / 2.0f + xJitter;
					centeredImageY = (float)y - _height / 2.0f + yJitter;

					Ray r = _cam->GetRayFromImageCoord(centeredImageX, centeredImageY);
					
					avgColor += SingleTrace(r,4,1);
				}
			}

			avgColor /= (float)avgCounter;

			*image.data(x, _height-1 - y, 0, 0) = avgColor.r;
			*image.data(x, _height-1 - y, 0, 1) = avgColor.g;
			*image.data(x, _height-1 - y, 0, 2) = avgColor.b;

		}
	}
	image.normalize(0, 255);
	image.save(destFileName.c_str());
	cimg_library::CImgDisplay disp(image, "lol");
	while (!disp.is_closed());
	disp.wait();


}

Scene::Intersection Scene::GetClosestIntersection(Ray r, bool shortCircuit)
{
	Scene::Intersection inter;
	inter.t = numeric_limits<float>::infinity();
	inter.source = NULL;
	for (int i = 0; i < _objects.size(); ++i) {
		float temp = _objects[i]->Intersect(r);
		if (temp > 0.0f && temp < inter.t) {
			inter.t = temp;
			inter.source = _objects[i];
			if (shortCircuit)
				break;
		}
	}

	return inter;
}

glm::vec3 Scene::SingleTrace(Ray r, int maxDepth, int currentDepth)
{
	glm::vec3 pixelColor(0.0f);
	Scene::Intersection inter = GetClosestIntersection(r);

	if (currentDepth < maxDepth  && inter.t != numeric_limits<float>::infinity() && inter.t > 0) {
		glm::vec3 intersectionPoint = r.LinearValue(inter.t);
		intersectionPoint += inter.source->GetNormal(intersectionPoint)  * (float)8e-4;
		float att = (float)currentDepth / (float)(maxDepth * 1.5);
		pixelColor += att * SingleTrace(Ray(intersectionPoint, inter.source->GetReflectedVector(r.direction*-1.0f, intersectionPoint)), maxDepth, currentDepth + 1);
	}

	if (inter.t != numeric_limits<float>::infinity() && inter.t > 0) {

		if (currentDepth == 1) {
			glm::vec3 intersectionPoint = r.LinearValue(inter.t);
			intersectionPoint += inter.source->GetNormal(intersectionPoint)  * (float)8e-4;

			for (int i = 0; i < _lights.size(); ++i) {
				Ray shadowRay(intersectionPoint, _lights[i]->GetDirectionFromPoint(intersectionPoint));
				Scene::Intersection interTemp = GetClosestIntersection(shadowRay, false);

				if (interTemp.t == numeric_limits<float>::infinity() || _lights[i]->IsBeforeIntersection(shadowRay, shadowRay.LinearValue(interTemp.t))) {
					inter.source->ApplyColorContribution(pixelColor, _lights[i]->GetColor() * 0.25f, shadowRay.direction, r.direction * -1.0f, intersectionPoint);
				}
				/*for (float yJitter = -1.5f; yJitter <= 1.5f; yJitter += 3.0f) {
					for (float xJitter = -1.5f; xJitter <= 1.5f; xJitter += 3.0f) {
						Ray shadowRay(intersectionPoint, _lights[i]->GetDirectionFromPoint(intersectionPoint, xJitter, yJitter));
						Scene::Intersection interTemp = GetClosestIntersection(shadowRay, false);

						if (interTemp.t == numeric_limits<float>::infinity() || _lights[i]->IsBeforeIntersection(shadowRay, shadowRay.LinearValue(interTemp.t))) {
							inter.source->ApplyColorContribution(pixelColor, _lights[i]->GetColor() * 0.25f, shadowRay.direction, r.direction * -1.0f, intersectionPoint);
						}
					}
				}*/
				
			}
		}
		inter.source->ApplyAmbientColorAndClamp(pixelColor);
	}
	return pixelColor;
}
