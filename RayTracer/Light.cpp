#include "Light.h"
#include "ReadingHelper.h"
#include<string>

using namespace std;

Light::Light()
{
}


Light::~Light()
{
}

glm::vec3 Light::GetDirectionFromPoint(glm::vec3 origin)
{
	return glm::normalize(_pos - origin);
}

glm::vec3 Light::GetDirectionFromPoint(glm::vec3 origin, float shiftX, float shiftY)
{
	glm::vec3 temp = _pos;
	temp.x += shiftX;
	temp.y += shiftY;
	return glm::normalize(temp - origin);
}

bool Light::IsBeforeIntersection(Ray r, glm::vec3 intersectionPoint)
{
	
	return glm::distance(r.origin, _pos) < glm::distance(r.origin, intersectionPoint);
}

void Light::LoadFromFile(std::ifstream & fs)
{
	string tempLine;
	for (int i = 1; i <= 2; i++) {
		getline(fs, tempLine, ':');

		if (tempLine == "pos") {
			_pos = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "col") {
			_color = ReadingHelper::ExtractVector(fs);
		}
	}

}
